import java.util.*;

public class DemoTest {

    public static void main(String[] args) {

       final double STARRATE = 10;
       final int NRATES = 6;
       final int NYEARS = 10;

       double[] interestRate = new double[NRATES];
        for (int j = 0; j < interestRate.length; j++) {
            interestRate[j] = (STARRATE + j) / 100.0;
        }

        double[][] balances = new double[NYEARS][NRATES];

        for (int j = 0; j < balances[0].length; j++) {
            balances[0][j] = 10000;
        }

        for (int i = 1; i < balances.length; i++) {
            for (int j = 0; j < balances[i].length; j++) {
                double oldBalance = balances[i - 1][j];
                double insert = oldBalance * interestRate[j];
                balances[i][j] = oldBalance + insert;
            }
        }

        for (int j = 0; j < interestRate.length; j++) {
            System.out.printf("%9.0f%%", 100 * interestRate[j]);
        }

    }




}
