import com.sun.imageio.plugins.common.ReaderUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Paarser {
    public static void main(String[] args) throws IOException {
        String [] urls = {"https://google.com", "https://www.facebook.com"};
        for (String url : urls) {
            String html = parse(url);
            System.out.println(html);
        }
        return;
    }

    private static String parse(String url) throws IOException {
        URL file = new URL(url);
        InputStream in = file.openStream();
        byte[] buffer = new byte[1024];
        StringBuilder res = new StringBuilder();

        while (in.available() > 0) {
            in.read(buffer);
            res.append(new String(buffer));
        }
        in.close();
        return res.toString();
    }
}
